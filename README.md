# 数字中国创新大赛-鲲鹏大赛  


### 关于大赛
数字中国是十九大提出的新时代国家信息化发展的新战略，福建福州是数字中国的实践起点和思想源头。由中央网信办、国家发改委、工信部和福建省政府联合主办的首届数字中国建设峰会于2018年4月在福州举行，福州市成为峰会的永久会址，2020年4月将迎来第三届峰会。

数字中国创新大赛是数字中国建设峰会的重要组成部分和标志性赛事，大赛与论坛和展会共同构成了峰会“一会、一展、一赛”三大主题活动。新一届数字中国创新大赛由数字中国建设峰会组委会主办，以“培育数字经济新动能，助推数字中国新发展”为主题，围绕核心前沿技术，以信息技术应用创新，设置了数字政府、智慧医疗、鲲鹏计算、网络安全四个赛道，是具有全国影响力的国内顶级赛事。

 “鲲鹏赛道”是数字中国创新大赛的“赛道”之一，同时也是创新大赛的热身赛和常设赛。赛道以国产化数字生态为主要方向，以国产处理器、服务器、操作系统、数据库、中间件等基础软硬件为支撑环境，以各类、各领域应用软件国产化开发、迁移、适配、调优为重点课题，面向企业和开发者，采取月赛和年度赛的形式，通过举办常态化保持、迭代式推进、高频度亮相的赛事、培训和交流活动，助推国产数字生态快速演进。


### 大赛日程
| 日程 | 时间 | 内容 |
| --- | --- | --- |
| 作品提交| 2020年1月10日 - 2020年3月1日| 报名及作品征集| 
| 作品初审| 2020年3月2日  - 2020年3月8日 | 评审内容：方案和设计| 
| 公布初审结果| 2020年3月9日| 公布初审入围作品名单| 
| 作品复审| 2020年4月5日| 评审内容：最终作品，已经上传完成开发且可独立运行的代码，或对开源代码实施国产环境迁移转化的成果| 
| 公布复审结果| 2020年4月20日| 公布复审入围作品名单及颁奖时间、地点| 
| 颁奖仪式 | 2020年数字中国建设峰会期间| 获奖项目颁奖| 

### 奖金池
总奖金池：50万（总决赛）  
一等奖：10w      
二等奖：2.5w     
三等奖：1w     
注：各类型的奖项名额将在初审后公布


### 大赛规则
大赛旨在推动完善国产数字生态。参赛者可结合自身技术栈、围绕完善国产化软件的生态体系，以鲲鹏为数字化底座，以自研或开源迁移的软件产品或技术框架作为参赛作品，将原依赖于非国产环境的开源软件作品迁移至国产环境中。

#### 作品类型
1.  基础软件类（例如操作系统、数据库、编程语言、云计算开发平台等）
2.  开发工具、框架和基础组件类（例如性能调优、IDE插件、流程图工具等）
3.  应用软件类（应用类可重点选择政务应用、大数据应用、区块链应用三个方向）

#### 作品要求
1.  参赛作品的运行环境原则上须基于鲲鹏处理器，以国产化服务器、操作系统、数据库、中间件等基础软硬件产品为支撑。
2.  参赛者须对参赛作品拥有自主知识产权，采用开放源码的部分，也必须在理解、消化、吸收基础上加以引用，做到安全、自主、可控。
3.  所有参赛作品要求开源并标注相关开源协议 


#### 如何参赛？

1.  准备作品  

- 在大赛仓库附件下载 1. 演示胶片、2.作品报名表
- Fork 大赛仓库至个人主页
- 完成演示胶片&作品报名表内容

2.  创建个人参赛仓库
- 仓库命名： 赛队名-作品名（例：吾爱开源-MusicAPP）
- 创建文件夹：作品演示
- 上传演示胶片至作品演示文件夹

3.  提交作品  
- 在个人主页找到 Fork 的大赛仓库  
- 选择作品类型对应的文件夹，上传作品
- 创建文件：赛队名-作品名
- 提交内容：参赛仓库：https:// gitee.com/xxxxxx
- 新建 Pull Requests 提交至 大赛官方仓库
- 将作品报名表发送至邮箱 kunpeng@fjsoft.org，作品命名规则统一为【赛队名-作品名】，大小不超过20M。  
  
关于 Fork 和 Pull Requests 的用法请阅读 https://gitee.com/help/articles/4128  
  
3.  完成作品  
在复审前上传完成代码至个人参赛仓库


#### 资料参考
1、鲲鹏处理器资料  
2、[代码一键迁移至 Gitee](https://blog.gitee.com/2019/10/11/github-gitee-organization/)   
3、如何使用 Gitee ？  
      [新手必读](https://gitee.com/help/categories/19) : 带您从零开始，如何用十分钟快速上手。   
      [操作手册](https://gitee.com/help/categories/36) : 90% 关于如何使用 Gitee 的疑问都可以在这里找到解答。   
4、....  
